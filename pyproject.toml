# SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

[build-system]
build-backend = 'setuptools.build_meta'
requires = ['setuptools >= 61.0', 'versioneer[toml]']

[project]
name = "demessaging"
dynamic = ["version"]
description = "python module wrapper for the data analytics software framework DASF"

readme = "README.md"
keywords = [
    "digital-earth",
    "dasf",
    "pulsar",
    "gfz",
    "hzg",
    "hereon",
    "hgf",
    "helmholtz",
    "remote procedure call",
    "rpc",
    "django",
    "python",
    "websocket",
]

authors = [
    { name = 'Daniel Eggert', email = 'daniel.eggert@gfz-potsdam.de' },
    { name = 'Mike Sips' },
    { name = 'Philipp S. Sommer', email = 'philipp.sommer@hereon.de' },
    { name = 'Doris Dransch' },
]
maintainers = [
    { name = 'Philipp S. Sommer', email = 'philipp.sommer@hereon.de' },
]
license = { text = 'Apache-2.0' }

classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Typing :: Typed",
]

requires-python = '>= 3.7'
dependencies = [
    # add your dependencies here
    "websocket-client>=1.5,<1.8",
    "docstring_parser<0.16",
    "pydantic>=2.2,<3.0",
    "pydantic-settings",
    "typing_extensions",
    "jinja2==3.1.2",
    "deprogressapi>=0.3.0",
    "PyYAML>=4.2b",
]

[project.urls]
Homepage = 'https://codebase.helmholtz.cloud/dasf/dasf-messaging-python'
Documentation = "https://dasf.readthedocs.io/en/latest/"
Source = "https://codebase.helmholtz.cloud/dasf/dasf-messaging-python"
Tracker = "https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/issues/"


[project.optional-dependencies]

backend = [
    "isort>=5.10.1",
    "black>=22.3.0",
    "autoflake>=2.0.0",
]

testsite = [
    "demessaging[backend]",
    "tox!=4.18.0",  # does not work with tox-current-env currently
    "isort==5.12.0",
    "black==23.1.0",
    "blackdoc==0.3.8",
    "flake8==6.0.0",
    "pre-commit",
    "mypy",
    "types-PyYAML",
    "dasf-broker-django",
    "daphne",
    "django-rest-framework",
    "djangorestframework",
    "pytest-django",
    "pytest-cov",
    "reuse",
    "xarray ; sys_platform != 'win32'",
    "scipy ; sys_platform != 'win32'",
    "cffconvert",
]
docs = [
    "autodocsumm",
    "sphinx-rtd-theme",
    "hereon-netcdf-sphinxext",
    "sphinx-argparse",
    "sphinx-design",
    "myst_parser",
]

dev = [
    "demessaging[testsite]",
    "demessaging[docs]",
    "reuse-shortcuts>=1.0.1",
    "types-PyYAML",
]



[project.scripts]
de-backend = "demessaging.__main__:_main"


[tool.mypy]
ignore_missing_imports = true
plugins = [
    "pydantic.mypy",
]

[tool.setuptools]
zip-safe = false
license-files = ["LICENSES/*"]

[tool.setuptools.package-data]
demessaging = [
    "py.typed",
    "config/logging.yaml",
    "templates/*.py.jinja2",
]

[tool.setuptools.packages.find]
namespaces = false
exclude = [
    'docs',
    'tests*',
    'examples'
]

[tool.pytest.ini_options]
addopts = '-v'
DJANGO_SETTINGS_MODULE = "testproject.settings"

[tool.versioneer]
VCS = 'git'
style = 'pep440'
versionfile_source = 'demessaging/_version.py'
versionfile_build = 'demessaging/_version.py'
tag_prefix = 'v'
parentdir_prefix = 'demessaging-'

[tool.isort]
profile = "black"
line_length = 79
src_paths = ["demessaging"]
float_to_top = true
known_first_party = "demessaging"

[tool.black]
line-length = 79
target-version = ['py38', 'py39', 'py310', 'py311']

[tool.coverage.run]
omit = ["demessaging/_version.py"]
