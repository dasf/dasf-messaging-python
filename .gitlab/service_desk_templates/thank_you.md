Thank you for your support request! We are tracking your request as ticket %{ISSUE_ID}, and will respond as soon as we can.

Please note, that your issue is created as confidential. So it will not be listed in the public issue tracker until the confidential flag is removed by a registered user.

You might send follow-up messages by replying to any eMail sent to you by the GitLab Support Bot.
