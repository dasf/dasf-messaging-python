# SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

"""
Example backend module for the de-messaging framework.

Start this module by typing::

    python ExampleMessageConsumer.py -t your-custom-topic connect

in the terminal, or::

    python ExampleMessageConsumer.py --help

for further options.
"""
import datetime
from typing import Any, Callable, Dict, List, Optional

import deprogressapi.tree
import xarray
from deprogressapi import ProgressReport
from pydantic import BaseModel

from demessaging import BackendModule as _BackendModule
from demessaging import configure, main, registry
from demessaging.config import ModuleConfig

NoneType = type(None)


__all__ = ["hello_world", "HelloWorld", "compute_sum", "report_progress_demo"]


@registry.register_type
class GreetResponse(BaseModel):
    message: str
    greetings: List[str]
    greeting_time: datetime.datetime


@configure(
    """
{
    "field_params": {
        "repeat": {
            "ge": 0
        }
    }
}
"""
)
def hello_world(
    message: str, repeat: int, greet_message: str
) -> GreetResponse:
    """
    Greet the hello world module.

    Parameters
    ----------
    message: str
        Message that will be part of the greet response
    repeat: int
        Number of repetitions of the greet message
    greet_message: str
        Message string that will be mirror back to the requesting node

    Returns
    -------
    GreetResponse
        Hello world greet response object
    """
    request = {
        "func_name": "hello_world",
        "message": message,
        "repeat": repeat,
        "greet_message": greet_message,
    }

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


class HelloWorld:
    """
    Greet the world from a class.

    Classes can define the methods that shall be used for the backend, and they
    define a constructor.
    """

    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            The hello message
        """
        self._request_base: Dict[str, Any] = {
            "class_name": "HelloWorld",
            "message": message,
        }

    @configure(
        """
    {
        "field_params": {
            "repeat": {
                "ge": 0
            }
        }
    }
    """
    )
    def repeat_message(self, repeat: int) -> List[str]:
        """
        Repeat the `message` multiple times.

        Parameters
        ----------
        repeat: int
            The number of times, `message` shall be repeated.
        """
        request = self._request_base.copy()
        request["function"] = {"func_name": "repeat_message", "repeat": repeat}

        model = BackendModule.model_validate(request)
        response = model.compute()

        return response.root  # type: ignore


@configure(
    """
{
    "validators": {
        "da": [
            "demessaging.validators.xarray.validate_dataarray"
        ]
    },
    "serializers": {
        "da": "demessaging.serializers.xarray.encode_xarray"
    },
    "return_validators": [
        "demessaging.validators.xarray.validate_dataarray"
    ],
    "return_serializer": "demessaging.serializers.xarray.encode_xarray"
}
"""
)
def compute_sum(
    da: xarray.core.dataarray.DataArray,
) -> xarray.core.dataarray.DataArray:
    """
    Compute the sum over a data array.

    Parameters
    ----------
    da : DataArray
        The input data array

    Returns
    -------
    DataArray
        The sum of the data array
    """
    request = {"func_name": "compute_sum", "da": da}

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


def report_progress_demo(
    reporter: Optional[deprogressapi.tree.ProgressReport] = ProgressReport(
        report_id="root", step_message="Demo report", steps=0, children=[]
    )
) -> None:
    """
    Demo function for a progress report with the dasf-progress-api.
    """
    request = {"func_name": "report_progress_demo", "reporter": reporter}

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


backend_config = ModuleConfig.model_validate_json(
    """
{
    "messaging_config": {
        "topic": "mytesttopic",
        "max_workers": 4,
        "queue_size": 4,
        "max_payload_size": 512000,
        "websocket_url": "ws://localhost:8000/ws/",
        "producer_url": "ws://localhost:8000/ws/",
        "consumer_url": "ws://localhost:8000/ws/"
}
}
"""
)

_creator: Callable
if __name__ == "__main__":
    _creator = main
else:
    _creator = _BackendModule.create_model

BackendModule = _creator(
    __name__,
    config=backend_config,
    class_name="BackendModule",
    members=[hello_world, HelloWorld, compute_sum, report_progress_demo],
)
