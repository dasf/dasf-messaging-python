<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2020-2021 Helmholtz-Zentrum Geesthacht GmbH
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC0-1.0
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.6.2] - 2025-02-27

### Added
- make send-request and compute accept plain json, see [!74](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/74)


## [v0.6.1] - 2025-02-27

### Fixed
- minor fix for api config, see [!69](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/69)


## [v0.6.0] - 2024-08-15

### Added

- implement compute cli command, see [!68](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/68)
- Added tests for pydantic 2.8, see [!67](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/67)
- implement listen options to dump requests or run arbitrary commands, see [!66](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/66)

## [v0.5.4] - 2024-06-03

### Fixed

- add description of classes, see [!65](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/65)

## [v0.5.3] - 2024-05-06

### Fixed
- compatibility hotfix for python 3.7, see [!64](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/63)

## [v0.5.2] - 2024-05-06

### Changed
- source code has been moved to https://codebase.helmholtz.cloud/dasf/dasf-messaging-python
- marked compatibility for python 3.7 and 3.8, see [!63](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/63)

## [v0.5.1] - 2024-04-03

### Changed
- renamed `--max_workers` command line option to `--max-workers`, see [!58](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/58)

### Added
- Added the option to modify the json schema for a model via the `configure` function, see [!59](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/59)


## [v0.5.0] - 2024-03-28

### Changed
- The implementation for the websocket connection for consumers and producers
  has been refactored to improve stability of long-living connections that we
  now use everywhere, see [!57](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/57)

## [v0.4.1] - 2024-03-21

### Changed
- Added PyYAML as dependency which was missing in `v0.4.0`


## [v0.4.0] - 2024-03-20

### Changed
- migrated to pydantic v2.2 (see [!50](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/50))

### Added
- A new message type has been added, `api_info`. The response to a message with
  this type will be an objects with `classes` and `functions` that are available
  by the module. See [!55](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/55).

### Removed
- the `demessaging.types` module has been in favor of a more explicit
  serialization and validation workflow for custom types. This affects the usage
  of `xarray` objects in particular. See
  [!51](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/merge_requests/51)
  for a migration guide.

## [v0.3.0] - 2022-10-20
### Added
- support for additional third-party message broker alongside Apache Pulsar
- authentication support
- developer documentation

### Changed
- major internal refactorings
- connection setup

## [v0.2.6] - 2022-05-20
### Changed
- internal refactorings

### Added
- producer ping-pong logic to avoid timeouts
- more tests
- more documentation

## [v0.2.5] - 2022-03-11
### Added
- extensive documentation at [https://dasf.readthedocs.io/en/latest/](https://dasf.readthedocs.io/en/latest/)

### Fixed
- topic override issue

## [v0.2.4] - 2022-02-05
### Changed
- default broker connection parameter to localhost:8080
- added progress api dependency to [backend]

### Fixed
- CI pipeline

## [v0.2.3] - 2022-02-04
### Added
- max_payload_size config parameter

## [v0.2.2] - 2022-02-03
### Fixed
- xarray encoding

## [v0.2.1] - 2022-01-13
### Added
- extented README.md documentation

### Fixed
- ci pipeline issues
- type checking issues

## [v0.2.0] - 2021-12-17
### Changed
- revised underlying rpc model
- minor refactorings

### Added
- progress reporting
