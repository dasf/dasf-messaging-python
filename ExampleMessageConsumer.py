# SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

"""Example backend module for the de-messaging framework.

Start this module by typing::

    python ExampleMessageConsumer.py -t your-custom-topic connect

in the terminal, or::

    python ExampleMessageConsumer.py --help

for further options.
"""
import datetime
from typing import List, Optional

# this import is (unfortunately) necessary to resolve the deprogressapi.tree in
# the generated client stub
import deprogressapi.tree  # noqa: F401
import xarray
from deprogressapi import ProgressReport
from pydantic import BaseModel

from demessaging import configure, main, registry
from demessaging.serializers.xarray import encode_xarray
from demessaging.validators.xarray import validate_dataarray

__all__ = ["hello_world", "HelloWorld", "compute_sum", "report_progress_demo"]


@registry.register_type
class GreetResponse(BaseModel):
    message: str
    greetings: List[str]
    greeting_time: datetime.datetime


# You can configure different validations for the parameters.
# Here, we know that `repeat` must at least be 0, so we set this as a field
# parameter (see
# https://pydantic-docs.helpmanual.io/usage/schema/#field-customisation)
# for available validators
@configure(field_params={"repeat": {"ge": 0}})
def hello_world(
    message: str, repeat: int, greet_message: str
) -> GreetResponse:
    """Greet the hello world module.

    Parameters
    ----------
    message: str
        Message that will be part of the greet response
    repeat: int
        Number of repetitions of the greet message
    greet_message: str
        Message string that will be mirror back to the requesting node

    Returns
    -------
    GreetResponse
        Hello world greet response object
    """
    greetings: List[str] = [greet_message] * repeat
    return GreetResponse(
        message=message,
        greetings=greetings,
        greeting_time=datetime.datetime.now(),
    )


# You can also use generic python classes as input/output for your model. In
# this case, we use xarrays DataArray. But we need to tell pydantic how it
# can be validated and serialized.
@configure(
    serializers={"da": encode_xarray},
    return_serializer=encode_xarray,
    validators={"da": [validate_dataarray]},
    return_validators=[validate_dataarray],
)
def compute_sum(da: xarray.DataArray) -> xarray.DataArray:
    """Compute the sum over a data array.

    Parameters
    ----------
    da : DataArray
        The input data array

    Returns
    -------
    DataArray
        The sum of the data array
    """
    ret = da.sum()
    return ret


def report_progress_demo(
    reporter: Optional[ProgressReport] = ProgressReport(
        step_message="Demo report"
    ),
) -> None:
    """Demo function for a progress report with the dasf-progress-api."""
    import time

    reporter = ProgressReport.from_arg(reporter)

    num_steps = 10
    reporter.steps = num_steps

    with reporter:
        for i in range(num_steps):
            with reporter.create_subreport(step_message=f"Sub report {i}"):
                time.sleep(1)
    return


# You can configure how the individual class in the backend module shall be
# interpreted with the `configure` function. See
# :class:`demessaging.config.backend.ClassConfig` for available parameters
@configure(methods=["repeat_message"])
class HelloWorld:
    """Greet the world from a class.

    Classes can define the methods that shall be used for the backend, and they
    define a constructor."""

    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            The hello message
        """
        self.message = message

    @configure(field_params={"repeat": {"ge": 0}})
    def repeat_message(self, repeat: int) -> List[str]:
        """Repeat the `message` multiple times.

        Parameters
        ----------
        repeat: int
            The number of times, `message` shall be repeated.
        """
        return [self.message] * repeat


if __name__ == "__main__":
    main(
        messaging_config=dict(
            topic="mytesttopic",
            max_workers=4,
            queue_size=4,
            websocket_url="ws://localhost:8000/ws/",
        )
    )
