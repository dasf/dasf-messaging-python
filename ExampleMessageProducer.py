# SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

import xarray as xr
from ExampleMessageProducerGen import GreetResponse, compute_sum, hello_world

response: GreetResponse = hello_world(
    message="Hello", greet_message="greetings", repeat=5
)
print(response)


sum = compute_sum(xr.DataArray([1, 2, 3]))
print(sum.load())
