<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(concept-rpc)=
# Remote Procedure Calls

The DASF Messaging Framework enables you to do so-called
_Remote Procedure Calls (RPC)_, meaning that you call a function on your
local computer and this function is then executed on a remote machine.

In the terminology of RPC, your local computer is the so-called _client stub_,
whereas the remote computer is the so-called _server stub_.

Because most people never heard of RPC, we often just name the _client stub_
client, and we call the _server stub_ the _backend module_.
