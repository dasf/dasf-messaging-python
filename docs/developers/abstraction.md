<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(abstraction)=
# Abstraction and Serialization of the backend module

Setting up a backend module within the DASF Framework can be done by every
newbie who just started with python. The basic API is very simple and a lot
happens under the hood. So this here is already a backend module

```{code-block} python
---
lineno-start: 1
caption: "`hello_world` function exposed via a HelloWorld backend module."
---
from demessaging import main


__all__ = ["hello_world"]


def hello_world(name: str) -> str:
    return 'Hello World, ' + name


if __name__ == "__main__":
    main()
```

You are probably familiar with these things (unless you are completely new to
python), but let's go through it and explain a bit, what happens here in the
background:

- `from demessaging import main`:

  This and the call of the
  {func}`~demessaging.backend.main` function is the only DASF specific part
  in this framework. We will come to this function below

- `__all__ = ["hello_world"]`

  This variable specifies, what should be available for the remote procedure
  call (see {ref}`abstraction-members`).

- `def hello_world(name: str) -> str:`

  This is the definition of a procedure that we want to make available as
  RPC through our framework. It's just a python function with type annotations
  (see {ref}`abstraction-procedures` for more details.)


- `return 'Hello World, ' + name`

  This is the computing part that happens in the server stub. In our case we
  just combine two strings, but you could do anything here that python let's
  you do.

- `main()`

  Now the magic happens. The {func}`~demessaging.backend.main` function that
  takes control now. It interprets your `hello_world` function and transforms
  it into something, that can be used to execute remote procedure calls (a
  subclass of [pydantics][pydantic] ``BaseModel``, we'll come to that later).
  It also generates a command-line interface that you can use to configure several
  aspects in DASF, see [the backend config](config-cli).

[pydantic]: https://pydantic-docs.helpmanual.io/

More in-depth details that describe what is happening behind the scenes
are provided in the following sections.

```{toctree}
---
maxdepth: 1
---
abstraction/module.md
abstraction/members.md
abstraction/procedures.md
```
