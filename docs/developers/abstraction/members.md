<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(abstraction-members)=
# Specifying module members

The first step in our creation of the backend module is to specify what
objects we want to make available for the remote procedure call. You find that
workflow in the source code of the backend modules
{meth}`~demessaging.backend.module.BackendModule.create_model` method.

{ref}`In our example <abstraction>` we used the `__all__` variable:

```python
__all__ = ["hello_world"]
```

In fact, there are two possible ways to specify what should be available for
the RPC:

1. using the `__all__` module variable (as we do above). The `__all__` variable
   is commonly used when using import statements like `from mymodule import *`
   (see the [python docs](https://docs.python.org/3/tutorial/modules.html#importing-from-a-package)),
   but it's also a good way to tell other developers about the important
   functions and methods in your code.
2. The second possibility is specifying the members in the call to the
   {func}`~demessaging.backend.main` function, e.g.
   `main(members=["hello_world"])` or `main(members=[hello_world])`. This would
   then take precedence over the `__all__` variable of the module.
