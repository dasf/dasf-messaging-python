<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(client-stubs)=
# Generating the client stub

Within DASF we provide the possibility to automatically generate the client stub from
the server stub (see {ref}`concept-rpc` if you want to know more about these
terms). This is done in the
{meth}`~demessaging.backend.module.BackendModule.generate` method of the
{class}`~demessaging.backend.module.BackendModule` class and can also be
invoked from the command line, e.g.

```bash
python ExampleMessageConsumer.py generate
```

The generation of these files work via so-called _jinja2_ templates[^jinja2].
Each config class, i.e.
{class}`~demessaging.config.backend.FunctionConfig`,
{class}`~demessaging.config.backend.ClassConfig` and
{class}`~demessaging.config.backend.ModuleConfig` holds the reference to a
{class}`~demessaging.template.Template`. This template is used to render the
corresponding function, class or module in the client stub. You can find these
templates in the [`demessaging/templates` folder of the source code][templates].

```{note}

currently we only support the automatic creation of python client stubs. But we
aim for the support of typescript in the near future.
```


[^jinja2]: https://pypi.org/project/Jinja2/

[templates]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/tree/master/demessaging/templates
