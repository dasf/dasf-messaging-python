<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2020-2021 Helmholtz-Zentrum Geesthacht GmbH
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

![DASF Logo](/_static/dasf_logo_v2_640.png)

# Welcome to DASF's documentation!

[![CI](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/badges/master/pipeline.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/pipelines?page=1&scope=all&ref=master)
[![Code coverage](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/badges/master/coverage.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/graphs/master/charts)
[![Docs](https://readthedocs.org/projects/dasf-messaging-python/badge/?version=latest)](https://dasf.readthedocs.io/en/latest/)
[![Latest Release](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python/-/badges/release.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-python)
[![PyPI version](https://img.shields.io/pypi/v/demessaging.svg)](https://pypi.python.org/pypi/demessaging/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/dasf/dasf-messaging-python)](https://api.reuse.software/info/codebase.helmholtz.cloud/dasf/dasf-messaging-python)
[![DOI](https://img.shields.io/badge/DOI-10.5880%2FGFZ.1.4.2021.005-blue)](https://doi.org/10.5880/GFZ.1.4.2021.005)
[![JOSS](https://joss.theoj.org/papers/e8022c832c1bb6e879b89508a83fa75e/status.svg)](https://joss.theoj.org/papers/e8022c832c1bb6e879b89508a83fa75e)

**The python module wrapper for the data analytics software framework DASF**

The **D**ata **A**nalytics **S**oftware **F**ramework DASF supports scientists to conduct data analysis in distributed IT infrastructures by sharing data analysis tools and data. For this purpose, DASF defines a remote procedure call (RPC) messaging protocol that uses a central message broker instance. Scientists can augment their tools and data with this protocol to share them with others or re-use them in different contexts.

## Language support
The DASF RPC messaging protocol is based on JSON and uses Websockets for the underlying data exchange. Therefore the DASF RPC messaging protocol in general is language agnostic, so all languages with Websocket support can be utilized. As a start DASF provides two ready-to-use language bindings for the messaging protocol, one for Python and one for the Typescript programming language.

## Needed infrastructure
DASF distributes messages via a central message broker. Currently we support a self-developed message broker called [dasf-broker-django](https://gitlab.hzdr.de/hcdc/django/dasf-broker-django), as well as an 'off-the-shelf' solution called [Apache Pulsar](https://pulsar.apache.org/). Both are quite versatile and highly configurable depending on your requirements. (also see: {ref}`messagebroker`)

```{toctree}
---
maxdepth: 2
caption: "Table of Contents"
---
usage/quickstart.md
api
developers.md
contributing
```

## How to cite this software

```{eval-rst}
.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff
```

## Open Source and Open Science



### License information

Copyright © 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
Copyright © 2020-2021 Helmholtz-Zentrum Geesthacht
Copyright © 2021-2025 Helmholtz-Zentrum hereon GmbH

The source code of dasf-messaging-python is licensed under Apache-2.0.

If not stated otherwise, the contents of this documentation is licensed
under CC-BY-4.0.

### Repository
The individual DASF modules are developed via the following git group. Feel free to checkout the source code or leave comment via the issue tracker.

```{admonition} Gitlab Repository URL
:class: note

[https://codebase.helmholtz.cloud/dasf](https://codebase.helmholtz.cloud/dasf)
```


### Acknowledgment
DASF is developed at the Helmholtz-Zentrum Hereon [https://www.hereon.de](https://www.hereon.de) with initial contributions by the GFZ German Research Centre for Geosciences ([https://www.gfz-potsdam.de](https://www.gfz-potsdam.de)) and was funded by the Initiative and Networking Fund of the Helmholtz Association through the Digital Earth project ([https://www.digitalearth-hgf.de/](https://www.digitalearth-hgf.de/)).
