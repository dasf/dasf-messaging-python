<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

# Developers Guide

If you want to learn more about the internals of the DASF messaging framework,
then welcome!

This part of the docs introduces you more deeply into

```{toctree}
---
maxdepth: 2
---
developers/rpc.md
developers/abstraction.md
developers/messaging.md
developers/client-stub.md
```

If there is missing something, please open an issue in the [gitlab repository][repo] (e.g. via the [service desk][service_desk]).
There are no dump questions, just bad documentation!

[repo]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-python
[service_desk]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-python#service-desk
