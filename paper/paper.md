<!--
SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

---
title: 'DASF: A data analytics software framework for distributed environments'
tags:
  - distributed
  - data analysis
  - framework
  - Python
  - Typescript
  - RPC
authors:
  - name: Daniel Eggert
    orcid: 0000-0003-0251-4390
    affiliation: 1
  - name: Mike Sips
    orcid: 0000-0003-3941-7092
    affiliation: 1
  - name: Philipp S. Sommer
    orcid: 0000-0001-6171-7716
    affiliation: 2
  - name: Doris Dransch
    affiliation: 1
affiliations:
 - name: Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
   index: 1
 - name: Helmholtz-Zentrum Hereon
   index: 2
date: 17 December 2021
bibliography: paper.bib
---

# Summary

The success of scientific projects increasingly depends on using data analysis tools and data in distributed IT infrastructures. Scientists need to use appropriate data analysis tools and data,
extract patterns from data using appropriate computational resources, and interpret the extracted patterns. Data analysis tools and data reside on different machines because the volume of the data often demands specific resources for their storage and processing, and data analysis tools usually require specific computational resources and run-time environments. The data analytics software framework `DASF`, which we develop in Digital Earth (@digitalearth), provides a framework for scientists to conduct data analysis in distributed environments.


# Statement of need

The data analytics software framework `DASF` supports scientists to conduct data analysis in distributed IT infrastructures by sharing data analysis tools and data. For this purpose, DASF defines a remote procedure call (RPC, @rpc) messaging protocol that uses a central message broker instance. Scientists can augment their tools and data with this protocol to share them with others. `DASF` supports many programming languages and platforms since the implementation of the protocol uses WebSockets. It provides two ready-to-use language bindings for the messaging protocol, one for Python and one for the Typescript programming language. In order to share a python method or class, users add an annotation in front of it. In addition, users need to specify the connection parameters of the message broker. The central message broker approach allows the method and the client calling the method to actively establish a connection, which enables using methods deployed behind firewalls. `DASF` uses Apache Pulsar (@pulsar) as its underlying message broker.

The Typescript bindings are primarily used in conjunction with web frontend components, which are also included in the `DASF-Web` library. They are designed to attach directly to the data returned by the exposed RPC methods. This supports the development of highly exploratory data analysis tools. `DASF` also provides a progress reporting API that enables users to monitor long-running remote procedure calls.

One application using the framework is the Digital Earth Flood Event Explorer (@fee). The Digital Earth Flood Event Explorer integrates several exploratory data analysis tools and remote procedures deployed at various Helmholtz centers across Germany.

# State of the field

DASF aims at connecting various data analysis tools and methods, as well as data visualization components in a distributed environment. A chain of connected analysis tools and visualizations can be seen as a data analysis workflow, so Workflow Engines like Galaxy (@galaxy), Kepler (@kepler), Taverna (@taverna) and Pegasus (@pegasus) could be used to model, implement and connect the individual tools and visualizations. Yet, the intellectual hurdles to be mastered when dealing with workflow systems are relatively high and the systems often do not offer much flexibility in case of distributed deployment of individual methods. In contrast, common Web Frameworks, like Django (@django), Flask (@flask) or GWT (@gwt) could also be used to connect multiple analysis methods and visualization components into an integrated data analysis chain, but usually such frameworks are limited to a single programming language, like Python or Java and only support a single backend and are therefore not well suited for analysis methods deployed in distributed infrastructures.

# Structure

The Data Analytics Software Framework (DASF) facilitates using data analysis tools in distributed IT infrastructures. The framework consists of three major modules:

`DASF-Web` (@dasfweb) collects all web components for the data analytics software framework DASF. It provides ready-to-use interactive data visualization components like time series charts, radar plots, stacked-parameter-relation (spr), and map components to support the visual analysis of spatio-temporal data. Moreover, `DASF-Web` includes the web bindings for the DASF RPC messaging protocol. It is implemented in Typescript and uses Vuejs/Vuetify, Openlayers and D3 as a technical basis.

`DASF-Messaging-Python` (@dasfmessaging) is a RPC (remote procedure call) wrapper library for the python programming language. As part of the data analytics software framework DASF, it implements the DASF RPC messaging protocol.

`DASF-Progress-API` (@dasfprogress) provides a lightweight tree-based structure to be sent via the DASF RPC messaging protocol. Its generic design supports deterministic as well as non-deterministic progress reports. While `DASF-Messaging-Python` provides the necessary implementation to distribute the progress reports from the reporting backend modules, `DASF-Web` includes ready-to-use components to visualize the reported progress.


# Acknowledgements

We acknowledge funding from the Initiative and Networking Fund of the Helmholtz Association through the project `Digital Earth`.

# References
