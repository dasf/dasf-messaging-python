# SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

"""Test backend module

This module exists just for test purposes
"""
import uuid

import xarray

from demessaging import configure, main
from demessaging.serializers.xarray import encode_xarray
from demessaging.validators.xarray import validate_dataarray

__all__ = ["compute_sum"]


topic = "test_topic_" + uuid.uuid4().urn[9:]


@configure(
    serializers={"da": encode_xarray},
    return_serializer=encode_xarray,
    validators={"da": [validate_dataarray]},
    return_validators=[validate_dataarray],
)
def compute_sum(da: xarray.DataArray) -> xarray.DataArray:
    """Compute the sum over a data array.

    Parameters
    ----------
    da : DataArray
        The input data array

    Returns
    -------
    DataArray
        The sum of the data array
    """
    return da.sum()


if __name__ == "__main__":
    main(messaging_config=dict(topic=topic))
