# SPDX-FileCopyrightText: 2019-2025 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

import os
import pathlib
import random
import subprocess as spr
import sys
import uuid
from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    Dict,
    Iterator,
    List,
    Optional,
    Type,
    cast,
)

import pytest
from _lazy_fixture import (  # noqa: F401
    LazyFixture,
    _resolve_lazy_fixture,
    is_lazy_fixture,
)
from pydantic import (
    BaseModel,
    ConfigDict,
    GetCoreSchemaHandler,
    field_serializer,
    field_validator,
)
from pydantic_core import core_schema

from demessaging import config, configure

try:
    import xarray
except ImportError:
    xarray = None  # type: ignore
else:
    try:
        import netCDF4
    except ImportError:
        netCDF4 = None  # type: ignore


os.environ["DASF_REPORT_SHOW_METHOD"] = "PRINT"


if TYPE_CHECKING:
    from _pytest.monkeypatch import MonkeyPatch


test_dir = pathlib.Path(__file__).parent


def pytest_make_parametrize_id(
    config: pytest.Config,
    val: object,
    argname: str,
) -> Optional[str]:
    """Inject lazy fixture parametrized id.

    Args:
        config (pytest.Config): pytest configuration.
        value (object): fixture value.
        argname (str): automatic parameter name.

    Returns:
        str: new parameter id.
    """
    if is_lazy_fixture(val):
        return cast(LazyFixture, val).name
    return None


@pytest.hookimpl(tryfirst=True)
def pytest_fixture_setup(
    fixturedef: pytest.FixtureDef,
    request: pytest.FixtureRequest,
) -> Optional[Any]:
    """Lazy fixture setup hook.

    This hook will never take over a fixture setup but just simply will
    try to resolve recursively any lazy fixture found in request.param.

    Reference:
    - https://bit.ly/3SyvsXJ

    Args:
        fixturedef (pytest.FixtureDef): fixture definition object.
        request (pytest.FixtureRequest): fixture request object.

    Returns:
        object | None: fixture value or None otherwise.
    """
    if hasattr(request, "param") and request.param:
        request.param = _resolve_lazy_fixture(request.param, request)
    return None


@pytest.fixture(scope="session")
def base_test_modules_path() -> pathlib.Path:
    return test_dir


@pytest.fixture(scope="session")
def get_test_module_path(
    base_test_modules_path: pathlib.Path,
) -> Callable[[str], str]:
    """Get the path to a test module"""

    def get_path(mod: str) -> str:
        return str(base_test_modules_path / (mod + ".py"))

    return get_path


@pytest.fixture(scope="session")
def requests_path(base_test_modules_path: pathlib.Path) -> pathlib.Path:
    return base_test_modules_path / "requests"


@pytest.fixture(scope="session")
def get_request_path(requests_path: pathlib.Path) -> Callable[[str], str]:
    """Get the path to a test module"""

    def get_path(request_name: str) -> str:
        return str(requests_path / (request_name + ".json"))

    return get_path


@pytest.fixture
def tmp_sys_path(tmpdir, monkeypatch: "MonkeyPatch") -> pathlib.Path:
    """Get a temporary path and add it to the sys.path."""
    sub = tmpdir.mkdir("sub")
    monkeypatch.syspath_prepend(str(sub))
    return sub


@pytest.fixture
def random_mod_name() -> str:
    """Generate a random module name."""
    return "test_" + str(random.randint(1, 10000))


@pytest.fixture
def tmp_module(tmp_sys_path: pathlib.Path, random_mod_name: str):
    """Generate a path to a module file for tests."""
    return tmp_sys_path / (random_mod_name + ".py")


@pytest.fixture
def func_sig() -> Callable[[int], List[int]]:
    """Create a test function with signature and docstring."""

    def func(a: int) -> List[int]:
        """A test function.

        With some docu.

        Parameters
        ----------
        a: integer
            An integer

        Returns
        -------
        list of int
            A list of integers
        """
        return [a]

    return func


@pytest.fixture
def func_json_schema_extra(
    func_sig: Callable[[int], List[int]]
) -> Callable[[int], List[int]]:
    configure(
        json_schema_extra={"testFunctionExtra": {"testFunction": "attribute"}}
    )(func_sig)
    return func_sig


@pytest.fixture
def func_invalid_sig() -> Callable[[int], List[int]]:
    """Create a test function with signature and docstring."""

    def func(a: int) -> List[int]:
        """A test function.

        With some docu.

        Parameters
        ----------
        a: integer
            An integer

        Returns
        -------
        list of int
            A list of integers
        """
        return a  # type: ignore

    return func


@pytest.fixture
def func_missing_doc() -> Callable[[int], int]:
    """Get a function with missing documentation."""

    def func(a: int) -> int:
        return a

    return func


@pytest.fixture
def func_missing_sig() -> Callable:
    """Get a function with missing signatures"""

    def func(a):
        """Do something.

        Parameters
        ----------
        a: int
            A parameter

        Returns
        -------
        int
            An integer
        """
        return a

    return func


def encode_arbitrary_type(a: "ArbitraryType") -> int:
    return a.a


class ArbitraryType:
    def __init__(self, a: int):
        self.a = a

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source: Type[Any], handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        assert source is ArbitraryType
        return core_schema.no_info_after_validator_function(
            cls, core_schema.int_schema()
        )


def validate_arbitrary_type(cls, v):
    if isinstance(v, ArbitraryType):
        return v
    else:
        return ArbitraryType(int(v))


class ArbitraryTestModel(BaseModel):
    """A model for test purposes."""

    model_config = ConfigDict(arbitrary_types_allowed=True)

    int_test_required: int

    int_test_optional: int = 1

    custom_type_required: ArbitraryType

    custom_type_optional: ArbitraryType = ArbitraryType(2)

    @field_validator("custom_type_required", "custom_type_optional")
    @classmethod
    def validate_custom_types(cls, v):
        return validate_arbitrary_type(cls, v)

    @field_serializer("custom_type_required", "custom_type_optional")
    def serialize_custom_types(self, v: ArbitraryType, _info):
        return encode_arbitrary_type(v)


config.registry.register_import(__name__)


default_registry = config.registry.model_copy()


@pytest.fixture(autouse=True)
def clean_registry() -> Iterator[config.ApiRegistry]:
    """Generate a clean registry for the test."""
    from demessaging import config

    saved_registry = config.registry

    config.registry = default_registry.model_copy(deep=True)

    yield config.registry

    config.registry = saved_registry


@pytest.fixture
def func_arbitrary_types() -> Callable[[ArbitraryType], ArbitraryType]:
    """Define a function with an arbitraty class as annotation."""

    @configure(
        serializers={"a": encode_arbitrary_type},
        return_serializer=encode_arbitrary_type,
        validators={"a": [validate_arbitrary_type]},
        return_validators=[validate_arbitrary_type],
    )
    def func(a: ArbitraryType) -> ArbitraryType:
        return a

    return func


@pytest.fixture
def func_arbitrary_model() -> (
    Callable[[ArbitraryTestModel], ArbitraryTestModel]
):
    """Define a function with an arbitrary model."""

    def func(a: ArbitraryTestModel) -> ArbitraryTestModel:
        return a

    return func


@pytest.fixture
def valid_obj() -> Dict[str, Any]:
    return {"a": 1}


@pytest.fixture
def invalid_obj() -> Dict[str, Any]:
    return {"a": "x"}


@pytest.fixture
def valid_arbitrary_model() -> Dict[str, ArbitraryTestModel]:
    return {
        "a": ArbitraryTestModel(
            int_test_required=1,
            custom_type_required=1,
        )
    }


@pytest.fixture
def default_class() -> Type[object]:
    """Create a test class with two methods."""

    class Class:
        """A test class."""

        def __init__(self, a: int, b: int = 1):
            """
            Parameters
            ----------
            a : int
                first integer
            b : int, optional
                second integer, by default 1
            """
            self.a = a
            self.b = b

        @staticmethod
        def sum(a: int, b: int) -> int:
            """
            Parameters
            ----------
            a : int
                first integer
            b : int, optional
                second integer, by default 1

            Returns
            -------
            int
                The sum of `a` and `b`
            """
            return a + b

        def _this_should_not_be_included(self, a: int) -> int:
            """This method should not be included because it stats with _

            Parameters
            ----------
            a: int
                Some dummy int
            """
            return a

        def add2a(self, c: int) -> int:
            """Add a number to `a`.

            Parameters
            ----------
            c : int
                The number to add

            Returns
            -------
            int
                a + c
            """
            return self.a + c

        def add2b(self, c: int) -> int:
            """Add a number to `b`

            Parameters
            ----------
            c : int
                The number to add

            Returns
            -------
            int
                b + c
            """
            return self.b + c

    return Class


@pytest.fixture
def class_json_schema_extra(default_class: Type[object]):
    """Test fixture for a class with json schema extra."""
    configure(
        json_schema_extra={"testClassExtra": {"testClass": "attribute"}}
    )(default_class)
    return default_class


@pytest.fixture
def class_method_json_schema_extra(default_class: Type[object]):
    """Test fixture for a class with json schema extra."""
    configure(
        json_schema_extra={"testMethodExtra": {"testMethod": "attribute"}}
    )(
        default_class.add2a  # type: ignore
    )
    return default_class


def create_brokertopic(slug: str):
    from dasf_broker.models import BrokerTopic
    from guardian.shortcuts import assign_perm, get_anonymous_user

    topic = BrokerTopic.objects.create(slug=slug, is_public=True)
    assign_perm("can_consume", get_anonymous_user(), topic)


@pytest.fixture
def topic(db) -> str:
    """Generate a random topic for testing."""
    topic_slug = "test_topic_" + uuid.uuid4().urn[9:]
    create_brokertopic(topic_slug)
    return topic_slug


def generate_test_module_command(
    test_module_path,
    topic: str,
    live_ws_server,
) -> List[str]:
    """Generate a command for subprocess to launch the test module."""
    return [
        sys.executable,
        test_module_path,
        "-t",
        topic,
        "--websocket-url",
        live_ws_server.ws_url + "/ws/",
    ]


@pytest.fixture
def test_module_command(
    test_module_path,
    topic: str,
    live_ws_server,
) -> List[str]:
    """Generate a command for subprocess to launch the test module."""
    return generate_test_module_command(
        test_module_path, topic, live_ws_server
    )


@pytest.fixture(scope="session")
def modified_env() -> Callable:
    """A fixture to generate a modified version of the current environ."""

    def modified_env(**extra_env):
        env = os.environ.copy()
        env.update(**extra_env)
        return env

    return modified_env


@pytest.fixture
def connected_module_basemodel(
    test_module_command_basemodel: List[str], spr_basemodel_env: Dict
) -> Iterator[spr.Popen]:
    """Generate a connection to the pulsar messaging system."""
    background_process = None

    try:
        background_process = spr.Popen(
            test_module_command_basemodel + ["listen"], env=spr_basemodel_env
        )
    except Exception:
        raise
    else:
        yield background_process
        background_process.terminate()


@pytest.fixture
def connected_module_report(
    test_module_command_report: List[str], spr_report_env: Dict
) -> Iterator[spr.Popen]:
    """Generate a connection to the pulsar messaging system."""
    background_process = None

    try:
        background_process = spr.Popen(
            test_module_command_report + ["listen"], env=spr_report_env
        )
    except Exception:
        raise
    else:
        yield background_process
        background_process.terminate()
